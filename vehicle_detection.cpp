#include <opencv2/opencv.hpp>
#include <iostream>
//#include <cv.h>
//#include <cxcore.h>
//#include <highgui.h>

const int KEY_SPACE = 32;
const int KEY_ESC = 27;

CvHaarClassifierCascade *cascade;
CvMemStorage            *storage;

//Street A
struct point
{
	int x;
	int y;
	uchar R;
	uchar G;
	uchar B;
};
point aa = { 500, 310, 0, 0, 0 };

void detect(IplImage *img, IplImage *img2);

int main(int argc, char** argv)
{
  std::cout << "Using OpenCV " << CV_MAJOR_VERSION << "." << CV_MINOR_VERSION << "." << CV_SUBMINOR_VERSION << std::endl;
  
  CvCapture *capture;
  IplImage  *frame;
  IplImage  *frameBasic;
  frameBasic = cvLoadImage("..//baseImage.bmp");
  uchar* ptr = (uchar*)(frameBasic->imageData + aa.y * frameBasic->widthStep);
  aa.B = *ptr++;
  aa.G = *ptr++;
  aa.R = *ptr;
  std::cout << "(B, G, R) = (" << aa.B << ", " << aa.G << ", " << aa.R << ")" << std::endl;
  //Make it green!
  //for (int y = 0; y<frameBasic->height; y++) {
	 // uchar* ptr = (uchar*)(frameBasic->imageData + y * frameBasic->widthStep);
	 // for (int x = 0; x<frameBasic->width; x++) {
		//  ptr[3 * x + 1] = 255; //Set red to max (BGR format)
	 // }
  //}

  std::cout << "frameBasic: " << frameBasic << std::endl;
  int input_resize_percent = 100;

  if(argc < 3)
  {
    std::cout << "Usage " << argv[0] << " cascade.xml video.avi" << std::endl;
	//getchar();
    return 0;
  }
  std::cout << "argv[0] = " << argv[0] << std::endl;
  std::cout << "argv[1] = " << argv[1] << std::endl;
  std::cout << "argv[2] = " << argv[2] << std::endl;

  if(argc == 4)
  {
    input_resize_percent = 100;
	std::cout << "atoi(argv[3]) = " << input_resize_percent << std::endl;
    std::cout << "Resizing to: " << input_resize_percent << "%" << std::endl;
  }
  cascade = (CvHaarClassifierCascade*) cvLoad(argv[1], 0, 0, 0);
  storage = cvCreateMemStorage(0);
  
  capture = cvCaptureFromFile(argv[2]);

  std::cout << "capture = " << capture << std::endl;
  assert(cascade && storage && capture);

  cvNamedWindow("video", 1);

  IplImage* frame1 = cvQueryFrame(capture);
  frame = cvCreateImage(cvSize((int)((frame1->width*input_resize_percent)/100) , (int)((frame1->height*input_resize_percent)/100)), frame1->depth, frame1->nChannels);
  int key = 0;
  do
  {
    frame1 = cvQueryFrame(capture);

    if(!frame1)
      break;

    cvResize(frame1, frame);

    detect(frame1, frameBasic);
    key = cvWaitKey(33);

    if(key == KEY_SPACE)
      key = cvWaitKey(0);

    if(key == KEY_ESC)
      break;

  }while(1);

  cvDestroyAllWindows();
  cvReleaseImage(&frame);
  cvReleaseCapture(&capture);
  cvReleaseHaarClassifierCascade(&cascade);
  cvReleaseMemStorage(&storage);

  return 0;
}

void detect(IplImage *img, IplImage *img2)
{
  CvSize img_size = cvGetSize(img);


  //Make it green!
  for (int y = aa.y - 2; y<aa.y + 2; y++) {
	uchar* ptr = (uchar*)(img->imageData + y * img->widthStep);
	uchar* ptr2 = (uchar*)(img2->imageData + y * img2->widthStep);
	for (int x = aa.x-2; x<aa.x+2; x++) {
		if (ptr2[3 * x] - ptr[3 * x] > 10 &&
				  ptr2[3 * x + 1] - ptr[3 * x + 1] > 10 &&
				  ptr2[3 * x + 2] - ptr[3 * x + 2] > 10)
		ptr[3 * x + 1] = 255; //Set green to max (BGR format)
	}
  }
  
  //for (int y = 0; y<img->height; y++) {
	 // uchar* ptr = (uchar*)(img->imageData + y * img->widthStep);
	 // uchar* ptr2 = (uchar*)(img2->imageData + y * img2->widthStep);
	 // for (int x = 0; x<img->width; x++) {
		//  if (ptr2[3 * x] - ptr[3 * x] > 0 ||
		//	  ptr2[3 * x + 1] - ptr[3 * x + 1] > 0 ||
		//	  ptr2[3 * x + 2] - ptr[3 * x + 2] > 0)
		//  {
		//	  std::cout << "ROZNICA" << std::endl;
		//	  ptr[3 * x + 2] = 255; //Set red to max (BGR format)
		//  }
	 // }
  //}
  

  //CvSeq *object = cvHaarDetectObjects(
  //  img,
  //  cascade,
  //  storage,
  //  1.1, //1.1,//1.5, //-------------------SCALE FACTOR
  //  1, //2        //------------------MIN NEIGHBOURS
	 // 0, //CV_HAAR_DO_CANNY_PRUNING
  //  cvSize(25,25),//cvSize( 30,30), // ------MINSIZE
  //  img_size //cvSize(70,70)//cvSize(640,480)  //---------MAXSIZE
  //  );

  //std::cout << "Total: " << object->total << " cars detected." << std::endl;
  //for(int i = 0 ; i < ( object ? object->total : 0 ) ; i++)
  //{
  //  CvRect *r = (CvRect*)cvGetSeqElem(object, i);
  //  cvRectangle(img,
  //    cvPoint(r->x, r->y),
  //    cvPoint(r->x + r->width, r->y + r->height),
  //    CV_RGB(255, 0, 0), 2, 8, 0);
  //}

  cvShowImage("video", img);
  //cvShowImage("video2", img2);
}